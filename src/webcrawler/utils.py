from urllib.parse import urljoin, urlparse, urldefrag, parse_qs,\
                        urlencode, urlunparse

class URLFilter:
    base_url = ''
    domain_restricted = False
    ignore_query = []

    def __init__(self, base_url, domain_restricted = False, ignore_query = []):
        self.base_url = base_url
        self.domain_restricted = domain_restricted
        self.ignore_query = ignore_query

    def fix(self, url, relative_url):
        """Cleans up the values we've extracted from hrefs
        
        This function attempts to remove any values that aren't page
        urls (e.g '#', 'mailto', 'ftp://') or don't originate in the base
        domain if domain_restricted is True. It will also make an attempt
        to correct relative urls and schemeless urls.
        """

        # Ditch urls with these prefixes
        invalid_prefixes = ('#', 'mailto:', 'ftp://')
        for prefix in invalid_prefixes:
            if url.startswith(prefix):
                return None

        # Deal with schemaless urls
        if url.startswith('//'):
            url = url[2:]
            url = 'https://' + url if relative_url.startswith('https://') else 'http://' + url;

        # Deal with relative urls
        if url.find('://') < 0:
            url = urljoin(relative_url, url)

        # Domain restrictions
        if self.domain_restricted and urlparse(url).hostname != urlparse(self.base_url).hostname:
            return None

        # Remove query params
        if len(self.ignore_query) > 0:
            parsed = urlparse(url)
            query = parse_qs(parsed.query)
            query = dict( (k, v) for k, v in query.items() if k not in self.ignore_query)
            url = urlunparse([
                parsed.scheme,
                parsed.netloc,
                parsed.path,
                parsed.params,
                urlencode(query),
                parsed.fragment
            ])

        url, frag = urldefrag(url)

        return url