import sys
import re
import requests
import time
from collections import deque

class Crawler:
    # Regular expression to extract href value from links
    ANCHOR_RE = r'<a href=[\'"]([^\'"]+)'

    url_fixer = None
    # Time to wait between each request
    sleep_time = 0
    # Queue to store urls
    urls = deque()
    # Set to store urls already crawled (has fast membership check)
    processed = set([])
    # Maximum number of pages to search
    max_pages = 1;
    # Number of pages searched
    page_count = 0
    # Term to find in page (can be reg ex)
    search_term = ''
    # List to store results
    results = []

    def __init__(self, base_url, max_pages, search_term,
                domain_restricted, url_filter):
        self.urls.append(base_url)
        self.max_pages = max_pages
        self.search_term = search_term
        self.url_filter = url_filter

    def crawl(self):
        # Process the queue until it's empty or we've reached the maximum
        # number of pages.
        while len(self.urls) > 0 and self.page_count < self.max_pages:
            url = self.urls.popleft()
            if url is None or url in self.processed:
                continue

            # Avoid getting 429 status codes
            if self.page_count > 0:
                time.sleep(self.sleep_time)

            print('Crawling ' + url)

            r = requests.get(url)
            if r.status_code == 200:
                body = r.text

                # Try and find the search term
                matches = self.search(body)
                if len(matches):
                    self.results.append({'url': url,
                                        'results': matches})

                # Extract links to crawl next
                links = re.findall(self.ANCHOR_RE, body)
                links = [self.url_filter.fix(link, url) for link in links]

                self.urls.extend(links)
                self.page_count += 1
            else:
                print('\033[91m' + 'ERROR: ' + url + ', status code: ' \
                        + str(r.status_code) + '\033[0m')

            # Store the url as visited regardless of the outcome
            self.processed.add(url)

        print('Processed ' + str(self.page_count) + ' pages')

    def search(self, text):
        results = []
        lines = enumerate(text.splitlines())

        for lineno, line in lines:
            matches = re.findall(self.search_term, line)
            if len(matches):
                results.append({ 'lineno': lineno+1, 'matches': matches })

        return results

    def get_results(self):
        return self.results