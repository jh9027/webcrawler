import sys
import json
from argparse import ArgumentParser

from crawler import Crawler
from utils import URLFilter

def main():
    parser = ArgumentParser(description='Simple crawler to search for a \
                            string within a webpage and the pages it links to.')
    parser.add_argument('url', metavar='url', type=str,
                        help='the url to start at')
    parser.add_argument('maxpages', metavar='maxpages', type=int,
                        help='the maximum number of pages to crawl')
    parser.add_argument('searchterm', metavar='term', type=str,
                        help='the string or regular expression to search for')
    parser.add_argument('-d', '--domainrestricted', action='store_true',
                        help='restrict the crawling to the same domain')
    parser.add_argument('-q', '--ignorequery', metavar='ignorequery', type=str,
                        help='a comma separated list of query parameters\
                         to remove e.g. utm_source')
    parser.add_argument('-f', '--outputfile', metavar='outputfile', type=str,
                        help='output the results to a file instead of stdout')
    opts = parser.parse_args()

    ignore_query = opts.ignorequery.split(',') if opts.ignorequery else []
    url_filter = URLFilter(opts.url, opts.domainrestricted, ignore_query)
    crawler = Crawler(opts.url, opts.maxpages, opts.searchterm,
                        opts.domainrestricted, url_filter)
    crawler.crawl()
    results = crawler.get_results()

    if opts.outputfile:
        with open(opts.outputfile, 'w') as f:
            json.dump(results, f, indent=4)
    else:
        print(json.dumps(results, indent=4))

if __name__ == "__main__":
    main()